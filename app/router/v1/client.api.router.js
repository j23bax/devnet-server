const expressRouter = require('express').Router();

const ClientController = require('../../controller/client.controller');

module.exports = () => {
  expressRouter.get('/list', ClientController.list);
  expressRouter.post('/new', ClientController.new);
  expressRouter.get('/detail', ClientController.detail);

  return expressRouter;
};