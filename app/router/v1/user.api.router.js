const expressRouter = require('express').Router();

// const User = require('../../model/user.model');
const UserController = require('../../controller/user.controller');

module.exports = () => {
  expressRouter.get('/list', UserController.list);
  expressRouter.post('/new', UserController.new);

  return expressRouter;
};