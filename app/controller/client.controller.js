const Client = require('../model/client.model');

module.exports = {
  list: (req, res) => {
    Client.find((err, clients) => {
      if (err) {
        console.error(err);
        res.sendStatus(400);
        return;
      }

      res.json(clients);
    });
  },
  new: (req, res) => {
    const newClient = new Client({
      firstName: req.body.client.firstName,
      lastName: req.body.client.lastName
    });
    newClient.save((err) => {
      if (err) {
        console.error(err);
        res.sendStatus(400);
        return;
      }

      res.json(newClient);
    });
  },
  detail: (req, res) => {
    res.json({status: "not-implemented"});
  }
};