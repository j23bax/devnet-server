var mongoose = require('../lib/mongoose.lib')(),
  Schema = mongoose.Schema;

const MessageSchema = new Schema({
  body: String,
  posted: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('Message', MessageSchema);