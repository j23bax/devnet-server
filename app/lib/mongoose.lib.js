const mongoose = require('mongoose');

module.exports = () => {
  mongoose.connect('mongodb://localhost/devnet-dev', {useMongoClient: true});
  mongoose.Promise = global.Promise;

  require('../model/user.model');
  require('../model/client.model');
  require('../model/message.model');

  return mongoose;
};