const bodyParser = require("body-parser"),
      morgan = require('morgan'),
      cors = require('cors');

const express = require("express");

const config = require("../../config/local");

module.exports = (routers, middlewares) => {
  const app = express();

  console.log(config.cors.allowedOrigin);
  // console.log(cors({origin: config.cors.allowedOrigin}));

  // app.options
  // app.use('*', cors({origin: config.cors.allowedOrigin}));
  const corsOptions = {
    origin: '*',
    methods: 'GET,POST,PATCH,PUT,DELETE,OPTIONS',
    preflightContinue: false,
    optionsSuccessStatus: 204
  };

  app.options('*', cors(corsOptions));
  app.use('*', cors(corsOptions));

  // middlewares
  app.use(bodyParser.json());
  app.use(morgan('tiny'));
  middlewares.forEach((middleware) => {
    app.use(middleware.path, middleware.middleware);
  }, this);

  app.get('/', (req, res) => res.send("Souris DevNet API"));
  routers.forEach((router) => {
    app.use(router.path, router.router);
  }, this);

  app.listen(4001, () => console.log("API server listen on port 4001"));

  return app;
};