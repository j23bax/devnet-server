module.exports = (req, res, next) => {
  if (req.headers['x-custom-authtoken'] !== undefined) {
    req.bearer = req.headers['x-custom-authtoken'];
  } else {
    req.bearer = undefined;
  }

  if (req.bearer !== "APIAPIAPIKEY") {
    res.sendStatus(401);
  } else {
    next();
  }
};